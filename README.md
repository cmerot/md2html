# md2html

Command line interface to generate html files from markdown files.

Special features:

- transform [PlantUML](http://plantuml.com/fr/) code into SVG
- index documents with [Elasticlunr](http://elasticlunr.com/), provide a very
  basic search interface via a React component.

I wrote this to have a quick preview of PlantUML diagrams in my code editor.
The search engine is just for fun.

It uses the [unified](https://unified.js.org/) ecosystem to process markdown and html.

## Usage

    Usage: md2html [options]

    Options:
    -V, --version             output the version number
    -w, --watch               watch for markdown changes
    -s, --add-search          add a search input
    -p, --transform-plantuml  process plantUML code blocks
    -o, --output-dir <dir>    define output directory
    -i, --input-dir <dir>     define input directory
    -h, --help                output usage information

## Install

    npm install -g @cmerot/md2html

## Development

### Files

- `./bin/md2html`: the CLI that generates html and copies the ./public content
  to the ouput directory
- `./lib`: the unified / remark / rehype processor libraries
- `./src`: the react app sources
- `./public/search-palette.js`: webpack bundle of the react app.

### Build the search-palette app

    yarn build
    yarn build:prod
    yarn watch
