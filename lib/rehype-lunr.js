const findAllAfter = require('unist-util-find-all-after')
const findAllBetween = require('unist-util-find-all-between')
const fs = require('fs')
const lunr = require('elasticlunr')
const toString = require('mdast-util-to-string')
const visit = require('unist-util-visit-parents')

let index = lunr(function() {
  this.addField('title')
  this.addField('body')
  this.setRef('id')
})

const attacher = () => {
  const transformer = (tree, file) => {
    const headings = []
    const visitor = (node, ancestors) => {
      headings.push({ ancestors, node })
    }
    visit(tree, 'heading', visitor)

    headings.forEach((heading, i) => {
      const nextHeading = headings[i + 1] ? headings[i + 1] : null
      const parent = heading.ancestors[heading.ancestors.length - 1]
      let body = ''
      try {
        if (nextHeading) {
          findAllBetween(parent, heading.node, nextHeading.node).forEach(
            node => {
              if (node !== heading.node) body += toString(node)
            }
          )
        } else {
          findAllAfter(parent, heading.node).forEach(node => {
            if (node !== heading.node) body += toString(node)
          })
        }
      } catch (error) {
        // One case of failure: heading.node and nextHeading.node don't have
        // the same direct parent. eg: https://raw.githubusercontent.com/postcss/postcss/8aba0a012c828d52b4f4a95b7536d530ccb1103a/docs/architecture.md
        file.message('Could not index properly')
      }
      file.data = file.data || {}
      file.data.lunr = file.data.lunr || []
      file.data.lunr.push({
        title: toString(heading.node),
        body: body.trim(),
        id: heading.node.data.id,
      })
    })
  }
  return transformer
}

const buildIndex = filename => {
  fs.writeFileSync(filename, 'var lunrIndex = ' + JSON.stringify(index), 'utf8')
}

const addDocument = doc => {
  if (doc.sections) {
    doc.sections.forEach(section => {
      index.addDoc({ ...section, id: `${doc.name}#${section.id}` })
    })
  }
}

module.exports = {
  attacher,
  buildIndex,
  addDocument,
}
