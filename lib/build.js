const chokidar = require('chokidar')
const emoji = require('remark-emoji')
const find = require('unist-util-find')
const format = require('rehype-format')
const fs = require('fs-extra')
const glob = require('glob')
const html = require('rehype-stringify')
const lunr = require('./rehype-lunr')
const markdown = require('remark-parse')
const mkdirp = require('mkdirp')
const path = require('path')
const rehypeDocument = require('rehype-document')
const remark2rehype = require('remark-rehype')
const report = require('vfile-reporter')
const slug = require('remark-slug')
const toc = require('remark-toc')
const toString = require('mdast-util-to-string')
const u = require('unist-builder')
const unified = require('unified')
const vfile = require('to-vfile')
const visit = require('unist-util-visit')

const plantuml = require('./remark-plantuml')

const PUBLIC_DIR = path.resolve(`${__dirname}'/../public`)
const INPUT_PATTERN = '**/*.md'

/**
 * Copy public files to the output directory. Takes a callback.
 */
const copyPublic = ({ outputDir }, cb) => {
  try {
    fs.copySync(PUBLIC_DIR, outputDir)
  } catch (error) {
    console.error(`Can't copy public to ${outputDir}`)
    process.exit(1)
  }
  if (cb) cb()
}

const copyImages = ({ inputDir, outputDir }) => (tree, file) => {
  visit(tree, 'image', node => {
    if (node.url.match(/^\..+/)) {
      const baseHref = getBaseHref(inputDir, file)
      const inputPath = path.join(file.path, baseHref, node.url)
      const outputPath = inputPath.replace(inputDir, outputDir)
      const rPath = path.relative(outputDir, outputPath)
      if (rPath.match(/^\.\.\//)) {
        file.message('Skip image outside of the input directory')
      } else {
        fs.copySync(inputPath, outputPath)
      }
    }
  })
}

/**
 * Return the relative path to the root
 */
const getBaseHref = (inputDir, file) => {
  const pathinfo = path.parse(file.path.replace(inputDir, ''))
  return pathinfo.dir === '/'
    ? '.'
    : pathinfo.dir.replace(/[^\/]+/g, '..').replace(/^\//, '')
}

/**
 * Remark plugin: copy first heading to file.data.title
 */
const extractDocTitle = () => (tree, file) => {
  const titleNode = find(
    tree,
    node => node.type === 'heading' && node.depth === 1
  )
  if (titleNode) {
    file.data.title = toString(titleNode)
  } else {
    file.data.title = file.name
    file.message(
      'No document title. Add a top level title to remove this warning.'
    )
  }
}

/**
 * Remark plugin: replace local links's extensions from .md to .html
 */
const fixMarkdownUrls = () => (tree, file) => {
  visit(tree, 'link', node => {
    if (!node.url.match(/^\..+\.md$/)) return
    node.url = node.url.replace(/md$/, 'html')
  })
}

/**
 * Rehype plugin: wrap the doc in div#content, add a wrapper for the search-palette
 * app
 */
const createAppLayout = () => tree => {
  const section = u('element', {
    tagName: 'div',
    properties: { id: 'content' },
    children: tree.children,
  })
  const app = u('element', {
    tagName: 'div',
    properties: { id: 'search-palette' },
  })
  tree.children = [app, section]
}

/**
 * Rehype plugin: add title, css and js
 */
const document = ({ inputDir, addSearch }) => (tree, file) => {
  const baseHref = getBaseHref(inputDir, file)
  const rehypeDocumentTransformer = rehypeDocument({
    title: file.data.title,
    css: [
      `${baseHref}/style.css`,
      'https://fonts.googleapis.com/css?family=Roboto',
    ],
    meta: {
      name: 'lunr-base-href',
      value: baseHref,
    },
    js: addSearch
      ? [`${baseHref}/lunr-index.js`, `${baseHref}/search-palette.js`]
      : [],
  })
  return rehypeDocumentTransformer(tree, file)
}

/**
 * Instanciate and return a unified processor
 */
const getProcessor = ({
  inputDir,
  outputDir,
  addSearch,
  transformPlantuml,
}) => {
  const processor = unified()
    // Load markdown
    .use(markdown)
    .use(fixMarkdownUrls)
    .use(copyImages, { inputDir, outputDir })
    .use(toc)
    .use(extractDocTitle)
    .use(emoji)

    // Index lunr
    .use(slug)

  if (addSearch) processor.use(lunr.attacher)

  if (transformPlantuml) processor.use(plantuml)

  return (
    processor
      // Transform to and update HTML
      .use(remark2rehype)
      .use(createAppLayout)

      // Add html, head and body tags
      .use(document, { inputDir, addSearch })
      .use(format)

      // To string
      .use(html)
  )
}

const buildFile = (processor, { inputDir, outputDir, addSearch }, inputFile) =>
  processor
    .process(vfile.readSync(inputFile))
    .then(file => {
      console.log(report(file))
      file.extname = '.html'
      file.path = file.path.replace(inputDir, outputDir)
      mkdirp.sync(path.dirname(file.path))
      vfile.writeSync(file)
      if (addSearch) {
        lunr.addDocument({
          name: file.path.replace(outputDir, ''),
          sections: file.data.lunr,
        })
      }
    })
    .catch(err => {
      console.error(report(err))
    })

const buildDir = (processor, options, cb) => {
  const { inputDir, outputDir, addSearch } = options
  glob([inputDir, INPUT_PATTERN].join('/'), (error, files) => {
    const processPromises = []
    files.forEach(inputFile => {
      processPromises.push(buildFile(processor, options, inputFile))
    })
    if (addSearch) {
      Promise.all(processPromises).then(() => {
        lunr.buildIndex(`${outputDir}/lunr-index.js`)
        if (cb) cb(options)
      })
    } else {
      if (cb) cb(options)
    }
  })
}

module.exports = options => {
  const processor = getProcessor(options)
  copyPublic(options)

  buildDir(processor, options, options => {
    if (options.watch) {
      const watcher = chokidar.watch(path.join(options.inputDir, INPUT_PATTERN))
      watcher.on('ready', () =>
        console.log(`Watching for changes in ${options.inputDir}`)
      )
      watcher.on('change', inputFile => {
        buildFile(processor, options, inputFile)
        if (options.addSearch) {
          lunr.buildIndex(`${options.outputDir}/lunr-index.js`)
        }
      })
    }
  })
}
