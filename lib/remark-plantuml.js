const plantuml = require('node-plantuml')
const visit = require('unist-util-visit')

const PLUGIN_NAME = 'remark-plantuml'

const plantumlToSVG = umlString => {
  const uml = plantuml.generate(umlString, { format: 'svg' })
  const chunks = []
  return new Promise((resolve, reject) => {
    uml.out.on('data', chunk => chunks.push(chunk))
    uml.out.on('error', reject)
    uml.out.on('end', data => {
      resolve(
        [
          'data:image/svg+xml;base64',
          Buffer.concat(chunks).toString('base64'),
        ].join(',')
      )
    })
  })
}

module.exports = () => async (tree, file) => {
  const nodesToChange = []
  visit(tree, 'code', (node, file) => {
    if (node.lang === 'plantuml') {
      nodesToChange.push({ node })
    }
  })
  for (const { node } of nodesToChange) {
    try {
      node.type = 'image'
      node.url = await plantumlToSVG(node.value)
    } catch (e) {
      file.message(error, node.position, PLUGIN_NAME)
      return reject(e)
    }
  }
}
