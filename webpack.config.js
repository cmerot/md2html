const path = require('path')

const OUTPUT_DIR = path.join(__dirname, 'public/')
const IS_PROD = process.env.NODE_ENV === 'production'

module.exports = {
  entry: {
    'search-palette': './src/index.js',
  },
  mode: IS_PROD ? 'production' : 'development',
  output: {
    path: OUTPUT_DIR,
    filename: '[name].js',
  },
}
