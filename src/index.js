import React from 'react'
import ReactDOM from 'react-dom'
import {
  Search,
  Palette,
  paletteClassName,
  examples,
} from '@cmerot/search-palette'

// FuzzySearch component uses the Search ReasonML component
// and it's dataSource, search and renderItem props also
// come from the Reason side.
// Because the search method requires a Reason specific structure
// for its dataSource, we also instanciate it on the Reason side.
const fuzzyDataSource = examples.getFuzzyDataSource(lunrIndex)
const FuzzySearch = React.createElement(Search.make, {
  ...examples.fuzzyEngine,
  dataSource: fuzzyDataSource,
})

// LunrSearch component
const lunrDataSource = examples.getLunrDataSource(lunrIndex)
const LunrSearch = React.createElement(Search.make, {
  ...examples.lunrEngine,
  dataSource: lunrDataSource,
})

// Get the app container and add a css class to it
const container = document.getElementById('search-palette')
container.className = paletteClassName

// Palette: a list of key/component pairs, also from the Reason
// side. Could be any component.
const DocPalette = React.createElement(Palette.make, {
  config: [
    { key: 'c', component: LunrSearch },
    { key: 'f', component: FuzzySearch },
  ],
})

ReactDOM.render(DocPalette, container)
